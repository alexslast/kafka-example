# kafka-example
C# middleware for working with Kafka.
Project contains simple service (**KafkaApp.Service**) that provides some info for client (**KafkaApp.Application**) via [Kafka](https://kafka.apache.org/). 
Interaction of client with back-end service is provided with middleware **KafkaApp.Broker**.

Before working with Kafka you should start the following services:
- Apache Zookeeper on 2181 port (default)
- Apache Kafka on 9092 port (default)

