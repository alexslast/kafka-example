﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KafkaApp.Constants
{
    public class BoyContants
    {
        // Topic for unwrapper (microservice) subscription
        public static string Topic { get { return "b_name_command"; } }
        
        // Topic for client subscription
        public static string TopicNameResp { get { return "b_name_response"; } }

        public static string MessageReq { get { return "get_boy_name"; } }        
    }
}
