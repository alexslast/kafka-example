﻿using Confluent.Kafka;
using KafkaApp.Constants;
using KafkaApp.Broker;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace KafkaApp.Application
{
    class Program
    {
        private static readonly string gTopicNameCmd = "g_name_command";
        private static readonly string gMessageReq = "get_girl_name";
        private static readonly string gTopicNameResp = "g_name_response";

        private static readonly string userHelpMsg = "MainApp: Enter 'b' for a boy or 'g' for a girl, 'q' to exit";

        // Timer for getting time necessary for executing method
        static System.Diagnostics.Stopwatch watch;

        static void Main(string[] args)
        {
            watch = System.Diagnostics.Stopwatch.StartNew();

            using (var msgBus = new ServiceBus())
            {
                Task.Run(() => msgBus.SubscribeOnTopic<string>(BoyContants.TopicNameResp, msg => GetBoyNameHandler(msg), CancellationToken.None));
                Task.Run(() => msgBus.SubscribeOnTopic<string>(gTopicNameResp, msg => GetGirlNameHandler(msg), CancellationToken.None));

                string userInput;

                do
                {
                    Console.WriteLine(userHelpMsg);
                    userInput = Console.ReadLine();

                    // Start timer
                    watch = System.Diagnostics.Stopwatch.StartNew();

                    switch (userInput)
                    {
                        case "b":
                            msgBus.SendMessage(topic: BoyContants.Topic, message: BoyContants.MessageReq);
                            break;
                        case "g":
                            msgBus.SendMessage(topic: gTopicNameCmd, message: gMessageReq);
                            break;
                        case "q":
                            break;
                        default:
                            Console.WriteLine($"Unknown command. {userHelpMsg}");
                            break;
                    }

                    // stop timer and show counted time
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;
                    Console.WriteLine("Time of sync: (don't forget to minus 5000 ms) " + elapsedMs);

                } while (userInput != "q");
            }
        }

        public static void GetBoyNameHandler(Message<Null, string> msg)
        {
            Console.WriteLine($"Boy name {msg.Value} is recommended");
        }

        public static void GetGirlNameHandler(Message<Null,string> msg)
        {
            Console.WriteLine($"Girl name {msg.Value} is recommended");
        }
    }
}
