﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Threading;

namespace KafkaApp.Broker
{
    public sealed class ServiceBus : IDisposable
    {
        private readonly IProducer<Null, string> _producer;
        private IConsumer<Null, string> _consumer;

        private readonly IDictionary<string, string> _producerConfig;
        private readonly IDictionary<string, string> _consumerConfig;

        public ServiceBus() : this("localhost") { }

        public ServiceBus(string host)
        {
            _producerConfig = new Dictionary<string, string>()
            {
                { "bootstrap.servers", host }
            };
            _consumerConfig = new Dictionary<string, string>
            {
                { "group.id", "custom-group"},
                { "bootstrap.servers", host }
            };

            _producer = new ProducerBuilder<Null, string>(_producerConfig).Build();
        }

        // Send message into topic
        public void SendMessage(string topic, string message)
        {
            var msg = new Message<Null, string>();
            msg.Value = message;
            _producer.ProduceAsync(topic, msg);
        }

        // Subscribe on topic       
        public void SubscribeOnTopic<T>(string topic, Action<Message<Null, string>> action, CancellationToken cancellationToken) where T : class
        {
            var msgBus = new ServiceBus();
            using (msgBus._consumer = new ConsumerBuilder<Null, string>(_consumerConfig).Build())
            {
                msgBus._consumer.Assign(new List<TopicPartitionOffset> { new TopicPartitionOffset(topic, 0, -1) });

                Message<Null, string> msg;
                ConsumeResult<Null, string> consumeResult;

                while (!cancellationToken.IsCancellationRequested)
                {                    
                    consumeResult = msgBus._consumer.Consume(TimeSpan.FromMilliseconds(10));
                    if (consumeResult != null)
                    {
                        msg = consumeResult.Message;
                        action(msg);
                    }
                }
            }
        }

        public void Dispose()
        {
            _producer?.Dispose();
            _consumer?.Dispose();
        }
    }
}
